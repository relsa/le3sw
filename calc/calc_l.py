# -*- coding: utf-8 -*-

import ply.lex as lex

tokens = (
        'INTEGER',
        )

literals = ['+', '*', '-']

def t_INTEGER(t):
    '[0-9]+'
    t.value = int(t.value)
    return t

t_ignore = ' \t\n'

def t_error(t):
    print("不正な文字 {}".format(t.value[0]))

lexer = lex.lex()

def l_test():
    data = input('data > ')
    lexer.input(data)
    for t in lexer:
        print(t)

