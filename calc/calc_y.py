# -*- coding: utf-8 -*-

import ply.yacc as yacc
from calc_l import tokens

def p_expression_program(p):
    'program : expr'
    print(p[1])

def p_expression_expr(p):
    ''' expr : mulexpr
             | expr '+' mulexpr
             | expr '-' mulexpr'''
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 4 and p[2] == '+':
        p[0] = p[1] + p[3]
    elif len(p) == 4 and p[2] == '-':
        p[0] = p[1] - p[3]

def p_expression_mulexpr(p):
    ''' mulexpr : INTEGER
                | mulexpr '*' INTEGER'''
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 4 and p[2] == '*':
        p[0] = p[1] * p[3]


def p_empty(p):
    'empty :'
    pass
    
def p_error(p):
    print("SyntaxError in input")

parser = yacc.yacc()

def y_test():
    while True:
        try:
            s = input('calc > ')
        except EOFError:
            break
        if not s:
            continue
        result = parser.parse(s)
        print(result)
