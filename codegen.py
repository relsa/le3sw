import sys
from tc_semantic import Obst
from syntax_tree import SyntaxTree as Tree

codelist = [] # 生成したコードを格納する
lblcount = {'for': 0, 'if': 0, 'while': 0, 'and': 0, 'or': 0, 'not': 0} # ラベルの管理
Nlocal = 0
return_to = None
running_loop = None

# コード生成関数
def emit(inst=None, op1=None, op2=None, index=None, label=None, comment=None):

    global codelist
    tmp = ""

    if not label is None:
        tmp += label + ':'

    if inst is None:
        pass
    elif op1 is None:
        tmp += "\t{0}".format(inst)
    elif op2 is None:
        tmp += "\t{0}\t{1}".format(inst, op1)
    else:
        tmp += "\t{0}\t{1}, {2}".format(inst, op1, op2)

    if comment is not None:
        return tmp
        tmp += "; {0}".format(comment)

    if index is not None:
        codelist.insert(index, tmp)
    else:
        codelist.append(tmp)

    return tmp

# 構文木読み取り関数
def emit_expr(tree):
    op = tree.op
    child = tree.child

    if op == 'CONS':
        for c in child:
            emit_expr(c)
    elif op == 'INT':
        for c in child:
            emit_expr(c)
    elif op == 'SET':
        set_codegen(tree)
    elif op == 'RET':
        ret_codegen(tree)
    elif op == 'NEG':
        neg_codegen(tree)
    elif op in ('PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'REMAINDER'):
        arithmetic_codegen(tree)
    elif op == 'OR':
        logical_or_codegen(tree)
    elif op == 'AND':
        logical_and_codegen(tree)
    elif op in ('EQ', 'NE', 'LT', 'GT', 'LE', 'GE'):
        comparison_codegen(tree)
    elif op == 'IF':
        if_codegen(tree)
    elif op == 'WHILE':
        while_codegen(tree)
    elif op == 'FUNC_DEF':
        func_codegen(tree)
    elif op == 'FBODY':
        for c in child:
            emit_expr(c)
    elif op == 'CMPD_STATE':
        for c in child:
            emit_expr(c)
    elif op == 'PARAM_LST':
        pass
    elif op == 'DECL':
        pass
    elif op == 'DECL_LST':
        pass
    elif op == 'STATE_LST':
        for c in child:
            emit_expr(c)
    elif op == 'FCALL':
        fcall_codegen(tree)
    elif op == 'CONSTANT':
        return constant_codegen(tree)
    elif op == 'ID':
        return id_codegen(tree)
    elif op == 'EXDECL':
        exdecl_codegen(tree)
    elif op == 'ASSIGN':
        for c in child:
            emit_expr(c)
    elif op == 'FOR':
        for_codegen(tree)
    elif op in ('ADDSET', 'SUBSET', 'MULSET', 'DIVSET'):
        arithset_codegen(tree)
    elif op == 'BREAK':
        break_codegen()
    elif op == 'CONTINUE':
        continue_codegen()
    elif op == 'NOT':
        logical_not_codegen(tree)
    elif op in ('F_INC', 'F_DEC'):
        former_inc_codegen(tree)
    elif op in ('L_INC', 'L_DEC'):
        latter_inc_codegen(tree)

# 関数定義の生成
def func_codegen(tree):
    emit(comment = '--- begin func_codegen ---')

    global Nlocal, return_to, codelist
    ftype_name, fparams, fbody = tree.child
    ftype = (ftype_name).op # 返り値の型
    fobst = emit_expr((ftype_name).child[0])
    fname = fobst.name
    Nlocal = - fobst.topalloc # topallocの符号反転

    begin_lbl, end_lbl = make_label(fname)[:2]
    return_to = end_lbl

    emit('GLOBAL', begin_lbl)
    emit('GLOBAL', end_lbl)
    emit(label=begin_lbl)
    emit('push', 'ebp', None)
    emit('mov', 'ebp', 'esp')
    index = len(codelist) # 関数内で局所変数を用いる場合に使用

    # 本体
    emit_expr(fbody)

    emit(label = end_lbl)
    if Nlocal > 0:
        # スタックにスペースを確保
        emit('sub', 'esp', Nlocal, index=index)
    emit('mov', 'esp', 'ebp')
    emit('pop', 'ebp')
    emit('ret')
    emit(comment = '--- end func_codegen ---')

# while文の生成
def while_codegen(tree):
    emit(comment = '--- begin while_codegen ---')

    cond, body = tree.child
    begin_lbl, end_lbl, cont_lbl = make_label('while')
    global running_loop
    running_loop = (cont_lbl, end_lbl)

    # 先頭ラベル
    emit(label=begin_lbl)
    emit(label=cont_lbl)

    # 条件式のコード生成
    emit_expr(cond)
    emit('cmp', 'eax', 0)
    emit('je', end_lbl)

    # 本体コード生成
    emit_expr(body)

    emit('jmp', begin_lbl)

    #末尾ラベル
    emit(label=end_lbl)
    running_loop = None
    emit(comment = '--- end while_codegen ---')

# break文の生成（拡張）
def break_codegen():
    global running_loop
    emit('jmp', running_loop[1])

# continue文の生成（拡張）
def continue_codegen():
    global running_loop
    emit('jmp', running_loop[0])

# for文の生成（拡張）
def for_codegen(tree):
    emit(comment = '--- begin for_codegen ---')

    init, cond, act, body = tree.child
    begin_lbl, end_lbl, cont_lbl = make_label('for')
    global running_loop
    running_loop = (cont_lbl, end_lbl)

    if init is not None:
        emit_expr(init)
    # 先頭ラベル
    emit(label=begin_lbl)

    # 条件式のコード生成
    if cond is not None:
        emit_expr(cond)
        emit('cmp', 'eax', 0)
        emit('je', end_lbl)

    # 本体コード生成
    emit_expr(body)
    emit(label=cont_lbl)

    if act is not None:
        emit_expr(act)
    emit('jmp', begin_lbl)

    #末尾ラベル
    emit(label=end_lbl)
    running_loop = None
    emit(comment = '--- end for_codegen ---')

# if文の生成
def if_codegen(tree):
    emit(comment = '--- begin if_codegen ---')

    cond, body, elbody = tree.child
    begin_lbl, end_lbl, else_lbl = make_label('if')

    # 先頭ラベル
    emit(label=begin_lbl)

    # 条件式のコード生成
    emit_expr(cond)
    emit('cmp', 'eax', 0)
    emit('je', else_lbl)

    # 本体コード生成
    emit_expr(body)
    emit('jmp', end_lbl)

    # elseコード生成
    emit(label=else_lbl)
    if elbody is not None:
        emit_expr(elbody)
    emit('jmp', end_lbl)

    #末尾ラベル
    emit(label=end_lbl)
    emit(comment = '--- end if_codegen ---')

# 算術演算式の生成
def arithmetic_codegen(tree):
    emit(comment = '--- begin arithmetic_codegen ---')
    global Nlocal
    idict = {'PLUS': 'add', 'MINUS': 'sub',
            'TIMES': 'imul', 'DIVIDE': 'idiv dword',
            'REMAINDER': 'idiv dword'}
    inst = idict.get(tree.op)
    left  = tree.child[0]
    right = tree.child[1]

    # 一時変数を用意
    Nlocal += 4
    tmp = "[ebp-{}]".format(Nlocal)

    # 右辺のコード生成
    emit_expr(right)
    emit("mov", tmp, "eax")

    # 左辺のコード生成
    emit_expr(left)
    if tree.op == 'DIVIDE':
        emit('cdq')
        emit(inst, tmp)
    if tree.op == 'REMAINDER':
        emit('cdq')
        emit(inst, tmp)
        emit('mov', 'eax', 'edx')
    else:
        emit(inst, 'eax', tmp)
    # Nlocal -= 4
    emit(comment = '--- end arithmetic_codegen ---')

# 符号反転演算子の生成
def neg_codegen(tree):
    emit(comment = '--- begin neg_codegen ---')
    n = tree.child[0]
    emit_expr(n)
    emit('neg', 'eax')
    emit(comment = '--- end neg_codegen ---')

# 比較文のコード生成
def comparison_codegen(tree):
    emit(comment = '--- begin comparison_codegen ---')
    global Nlocal
    idict = {'LT': 'setl', 'LE': 'setle', 'GT': 'setg',
            'GE': 'setge', 'NE': 'setne', 'EQ': 'sete'}
    inst = idict.get(tree.op)
    left  = tree.child[0]
    right = tree.child[1]

    # 一時変数を用意
    Nlocal += 4
    tmp = '[ebp-{}]'.format(Nlocal)

    # 右辺のコード生成
    emit_expr(right)
    emit('mov', tmp, 'eax')

    # 左辺のコード生成
    emit_expr(left)

    emit('cmp', 'eax', tmp)
    emit(inst, 'al')
    emit('movzx', 'eax', 'al')
    emit(comment = '--- end comparison_codegen ---')

# 論理積の生成
def logical_and_codegen(tree):
    emit(comment = '--- begin logical_and_codegen ---')

    global Nlocal
    left  = tree.child[0]
    right = tree.child[1]
    lbl = make_label('and')[0]

    # 一時変数を用意
    Nlocal += 4
    tmp = '[ebp-{}]'.format(Nlocal)

    emit('mov dword', tmp, 0)

    # 左項の評価
    emit_expr(left)
    emit('cmp', 'eax', 0)
    emit('je', lbl)

    # 右項の評価
    emit_expr(right)
    emit('cmp', 'eax', 0)
    emit('je', lbl)

    emit('mov dword', tmp, 1)

    emit(label = lbl)
    emit('mov', 'eax', tmp)
    # Nlocal -= 4
    emit(comment = '--- end logical_and_codegen ---')

# 論理和の生成
def logical_or_codegen(tree):
    emit(comment = '--- begin logical_or_codegen ---')

    global Nlocal
    left  = tree.child[0]
    right = tree.child[1]
    lbl = make_label('or')[0]

    # 一時変数を用意
    Nlocal += 4
    tmp = '[ebp-{}]'.format(Nlocal)

    emit('mov dword', tmp, 1)

    # 左項の評価
    emit_expr(left)
    emit('cmp', 'eax', 0)
    emit('jne', lbl)

    # 右項の評価
    emit_expr(right)
    emit('cmp', 'eax', 0)
    emit('jne', lbl)

    emit('mov dword', tmp, 0)

    emit(label = lbl)
    emit('mov', 'eax', tmp)
    # Nlocal -= 4
    emit(comment = '--- end logical_or_codegen ---')

# 論理否定の生成（拡張）
def logical_not_codegen(tree):
    emit(comment = '--- begin not_codegen ---')
    sentence = tree.child[0]
    lbl0, lbl1 = make_label('not')[:2]

    emit_expr(sentence)
    emit('cmp', 'eax', 0)
    emit('je', lbl0)

    emit('mov', 'eax', 0)
    emit('jmp', lbl1)

    emit(label=lbl0)

    emit('mov', 'eax', 1)

    emit(label=lbl1)

    emit(comment = '--- end not_codegen ---')



# 関数呼び出しの生成
def fcall_codegen(tree):
    emit(comment = '--- begin fcall_codegen ---')
    fobst = tree.child[0].child[0]
    param_tree = tree.child[1]

    # 未定義関数はEXTERN命令が必要
    if fobst.kind == 'UNDEF_FUN':
        emit('EXTERN', '_' + fobst.name)

    def push_params(tree):
        if tree == None:
            pass
        elif tree.op == 'CONS':
            for n in reversed(tree.child):
                push_params(n)
        else:
            emit_expr(tree)
            emit('push', 'eax')


    push_params(param_tree)

    if fobst.kind == 'UNDEF_FUN':
        emit('call', '_' + fobst.name)
    else:
        emit('call', fobst.name)
    emit('add', 'esp', 4 * fobst.argnum)
    emit(comment = '--- end fcall_codegen ---')

# 代入文の生成
def set_codegen(tree):
    emit(comment = '--- begin set_codegen ---')
    bind_var = (tree.child[0]).child[0]
    val = emit_expr(tree.child[1])

    dest = loc(bind_var)
    # val = loc(val)

    if dest == 'VAR':
        emit('mov', '[{}]'.format(bind_var.name), 'eax')
    elif dest == 'FUN':
        pass
    else:
        emit('mov', dest, 'eax')
    emit(comment = '--- end set_codegen ---')

# 演算代入文の生成
def arithset_codegen(tree):
    emit(comment = '--- begin arithset_codegen ---')
    left = tree.child[0]
    bind_var = left.child[0]
    right = tree.child[1]
    dest = loc(bind_var)

    idict = {'ADDSET': 'add', 'SUBSET': 'sub',
            'MULSET': 'imul', 'DIVSET': 'idiv'}
    inst = idict.get(tree.op)

    # 一時変数を用意
    global Nlocal
    Nlocal += 4
    tmp = "[ebp-{}]".format(Nlocal)

    # 右辺のコード生成
    emit_expr(right)
    emit("mov", tmp, "eax")

    # 左辺のコード生成
    emit_expr(left)
    if inst == 'idiv':
        emit('cdq')
        emit('idiv dword', tmp)
    else:
        emit(inst, 'eax', tmp)

    if dest == 'VAR':
        emit('mov', '[{}]'.format(bind_var.name), 'eax')
    elif dest == 'FUN':
        pass
    else:
        emit('mov', dest, 'eax')
    emit(comment = '--- end arithset_codegen ---')

# return文の生成
def ret_codegen(tree):
    emit(comment = '--- begin ret_codegen ---')
    global return_to
    return_val = emit_expr(tree.child[0])
    emit('jmp', return_to)
    emit(comment = '--- end ret_codegen ---')

# 定数の処理
def constant_codegen(tree):
    emit(comment = '--- begin constant_codegen ---')
    const = tree.child[0]
    emit('mov', 'eax', const)
    emit(comment = '--- end constant_codegen ---')
    return const

# 変数・関数名の処理
def id_codegen(tree):
    emit(comment = '--- begin id_codegen ---')
    identifier = tree.child[0]
    locate = loc(identifier)
    if locate == 'VAR':
        emit('mov', 'eax', '[{}]'.format(identifier.name))
    elif locate == 'FUN':
        pass
    else:
        emit('mov', 'eax', loc(identifier))
    emit(comment = '--- end id_codegen ---')
    return identifier

# global変数の生成
def exdecl_codegen(tree):

    emit(comment = '--- begin exdecl_codegen ---')
    d = tree.child[0].child[0] # INT decl_list
    l = []

    def search_id(tree):
        nonlocal l
        if isinstance(tree, Tree) and tree.op == 'ID':
            l.append(tree.child[0])
        elif isinstance(tree, Tree):
            for c in tree.child:
                search_id(c)
        else:
            pass

    search_id(tree)

    for o in l:
        emit('COMMON {}'.format(o.name), 4)

    emit(comment = '--- end exdecl_codegen ---')

# 前置インクリメント・デクリメント
def former_inc_codegen(tree):
    idict = {'F_INC': 'inc', 'F_DEC': 'dec'}
    op = idict.get(tree.op, '')
    var = tree.child[0]
    dest = loc(var.child[0])
    emit(op + ' dword', dest)
    emit_expr(var)

# 後置インクリメント・デクリメント
def latter_inc_codegen(tree):
    idict = {'L_INC': 'inc', 'L_DEC': 'dec'}
    op = idict.get(tree.op, '')
    var = tree.child[0]
    emit_expr(var)
    dest = loc(var.child[0])
    emit(op + ' dword', dest)

# ラベルの生成
def make_label(lbl):
    global lblcount
    cnt = lblcount.get(lbl, '')
    t = '{0}{1}'.format(lbl, cnt)
    begin_lbl =  t
    end_lbl   = 'end' + t
    else_lbl  = 'el' + t
    if cnt != '':
        lblcount[lbl] += 1
    return (begin_lbl, end_lbl, else_lbl)

# スタック割り当て
def loc(ob):
    if isinstance(ob, Obst):
        off = ob.offset
        kind = ob.kind
        if off is None:
            return kind
        if off > 0:
            return "[ebp+{0}]".format(off)
        elif off < 0:
            return "[ebp{0}]".format(off)
        else:
            sys.stderr.write('Locating Error: wrong offset {}\n'.format(off))
    elif isinstance(ob, int):
        return ob
    else:
        return 'eax'

# コードの表示
def print_code(tree):
    global codelist
    emit_expr(tree)
    for l in codelist:
        print(l)

