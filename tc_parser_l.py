import ply.lex as lex
import sys

tokens = [
        'IDENTIFIER',
        'CONSTANT',
        'PLUS',
        'MINUS',
        'TIMES',
        'LPAREN',
        'RPAREN',
        'LBRACE',
        'RBRACE',
        'DIVIDE',
        'AND',
        'OR',
        'EQ',
        'NE',
        'LT',
        'GT',
        'LE',
        'GE',
        'COMMA',
        'SCOLON',
        'SET',
        'PSET',
        'MSET',
        'TSET',
        'DSET',
        'REMAINDER',
        'NOT',
        'INC',
        'DEC'
        ]

reserved = {
        'if': 'IF',
        'else': 'ELSE',
        'while': 'WHILE',
        'int': 'INT',
        'return': 'RETURN',
        'for': 'FOR',
        'break': 'BREAK',
        'continue': 'CONTINUE'
        }

# 予約語を追加
tokens = tokens + list(reserved.values())

t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_REMAINDER = r'%'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_AND = r'&&'
t_OR = r'\|\|'
t_EQ = r'=='
t_NE = r'!='
t_LT = r'<'
t_GT = r'>'
t_LE = r'<='
t_GE = r'>='
t_COMMA = r','
t_SCOLON = r';'
t_SET = r'='
t_PSET = r'\+='
t_MSET = r'-='
t_TSET = r'\*='
t_DSET = r'/='
t_NOT = r'!'
t_INC = r'\+\+'
t_DEC = r'--'

def t_IDENTIFIER(t):
    r'[A-Za-z][A-za-z0-9_]*'
    t.type = reserved.get(t.value, 'IDENTIFIER')
    return t

def t_CONSTANT(t):
    r'\d+'
    t.value = int(t.value)
    return t


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

t_ignore = ' \t'

def t_error(t):
    sys.stderr.write("Illegal character: '{}'\n".format(t.value[0]))

lexer = lex.lex()

def l_test():
    while True:
        data = input("data > ")

        if data is None:
            break
        else:
            lexer.input(data)

        for t in lexer:
            print(t)

