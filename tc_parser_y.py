import ply.yacc as yacc
import sys
from syntax_tree import SyntaxTree as Tree
from syntax_tree import print_tree
from tc_parser_l import tokens
from tc_semantic import *
from codegen import print_code

# 構文上の深さを表す
cur_lev = 0

def p_program(p):
    '''program : external_declaration
               | program external_declaration'''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = Tree('CONS', p[1], p[2])


def p_external_declaration(p):
    '''external_declaration : declaration empty
                            | function_definition'''
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 3:
        p[0] = Tree('EXDECL', p[1])

def p_declaration(p):
    '''declaration : INT declarator_list SCOLON'''
    p[0] = Tree('DECL', Tree('INT', p[2]))

def p_declarator_list(p):
    '''declarator_list : declarator
                       | declarator_list COMMA declarator'''
    global cur_lev
    if len(p) == 2:
        p[0] = Tree('ID', make_var_decl(p[1], cur_lev))
    else:
        p[0] = Tree('CONS', p[1], Tree('ID', make_var_decl(p[3], cur_lev)))

def p_declarator(p):
    '''declarator : IDENTIFIER'''
    global cur_lev
    p[0] = make_Obst(p[1], cur_lev)

def p_function_name(p):
    '''function_name : IDENTIFIER'''
    global cur_lev
    p[0] = make_fun_decl(make_Obst(p[1], cur_lev))

def p_function_definition(p):
    '''function_definition : INT function_name LPAREN inc_lev parameter_type_list RPAREN f_compound_statement
                           | INT function_name LPAREN inc_lev RPAREN f_compound_statement'''
    if len(p) == 8:
        p[0] = Tree('FUNC_DEF', Tree('INT', Tree('ID', p[2])), p[5], p[7])
    else:
        p[0] = Tree('FUNC_DEF', Tree('INT', Tree('ID', p[2])), None, p[6])

def p_parameter_type_list(p):
    '''parameter_type_list : parameter_declaration
                           | parameter_type_list COMMA parameter_declaration'''
    if len(p) == 2:
        p[0] = Tree('PARAM_LST', p[1])
    else:
        p[0] = Tree('PARAM_LST', p[1], p[3])


def p_parameter_declaration(p):
    '''parameter_declaration : INT declarator'''
    global cur_lev
    p[0] = Tree('INT', Tree('ID', make_param_decl(p[2], cur_lev)))

def p_statement(p):
    '''statement : SCOLON
                 | expression SCOLON
                 | compound_statement
                 | IF LPAREN expression RPAREN statement
                 | IF LPAREN expression RPAREN statement ELSE statement
                 | WHILE LPAREN expression RPAREN statement
                 | RETURN expression SCOLON
                 | FOR LPAREN for_expression SCOLON for_expression SCOLON for_expression RPAREN statement
                 | BREAK SCOLON
                 | CONTINUE SCOLON'''
    if len(p) == 2 and p[1] == ';': # 規則1
        p[0] = None
    elif len(p) == 2 and p[1] != ';': # 規則3
        p[0] = p[1]
    elif len(p) == 3 and p[1] == 'break':
        p[0] = Tree('BREAK', None)
    elif len(p) == 3 and p[1] == 'continue':
        p[0] = Tree('CONTINUE', None)
    elif len(p) == 3: # 規則2
        p[0] = p[1]
    elif len(p) == 4: # 規則7
        p[0] = Tree('RET', p[2])
    elif len(p) == 6 and p[1] == 'if': # 規則4
        p[0] = Tree('IF', p[3], p[5], None)
    elif len(p) == 6 and p[1] == 'while': # 規則6
        p[0] = Tree('WHILE', p[3], p[5])
    elif len(p) == 10 and p[1] == 'for': # 規則6
        p[0] = Tree('FOR', p[3], p[5], p[7], p[9])
    else: # 規則5
        p[0] = Tree('IF', p[3], p[5], p[7])


def p_compound_statement(p):
    '''compound_statement : LBRACE inc_lev declaration_list statement_list RBRACE dec_lev
                          | LBRACE inc_lev statement_list RBRACE dec_lev
                          | LBRACE inc_lev declaration_list RBRACE dec_lev
                          | LBRACE inc_lev RBRACE dec_lev'''
    if len(p) == 7: # 規則1
        p[0] = Tree('CMPD_STATE', p[3], p[4])
    elif len(p) == 6: # 規則2, 3
        p[0] = p[3]
    else:
        p[0] = None

def p_f_compound_statement(p):
    '''f_compound_statement : LBRACE inc_lev declaration_list statement_list RBRACE dec_lev dec_lev
                            | LBRACE inc_lev statement_list RBRACE dec_lev dec_lev
                            | LBRACE inc_lev declaration_list RBRACE dec_lev dec_lev
                            | LBRACE inc_lev RBRACE dec_lev dec_lev'''
    if len(p) == 8: # 規則1
        p[0] = Tree('FBODY', p[3], p[4])
    elif len(p) == 7: # 規則2, 3
        p[0] = p[3]
    else:
        p[0] = None

def p_declaration_list(p):
    '''declaration_list : declaration
                        | declaration_list declaration'''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = Tree('DECL_LST', p[1], p[2])

def p_statement_list(p):
    '''statement_list : statement
                      | statement_list statement'''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = Tree('STATE_LST', p[1], p[2])

def p_expression(p):
    '''expression : assign_expr
                  | expression COMMA assign_expr'''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = Tree('CONS', p[1], p[2])

def p_for_expression(p):
    '''for_expression : empty
                      | assign_expr
                      | for_expression COMMA assign_expr'''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = Tree('CONS', p[1], p[2])

def p_assign_expr(p):
    '''assign_expr : logical_OR_expr
                   | IDENTIFIER SET assign_expr
                   | IDENTIFIER PSET assign_expr
                   | IDENTIFIER MSET assign_expr
                   | IDENTIFIER TSET assign_expr
                   | IDENTIFIER DSET assign_expr'''
    if len(p) == 2:
        p[0] = p[1]
    elif p[2] == '=':
        p[0] = Tree('SET', Tree('ID', ref_var(make_Obst(p[1], cur_lev))), p[3])
    elif p[2] == '+=':
        p[0] = Tree('ADDSET', Tree('ID', ref_var(make_Obst(p[1], cur_lev))), p[3])
    elif p[2] == '-=':
        p[0] = Tree('SUBSET', Tree('ID', ref_var(make_Obst(p[1], cur_lev))), p[3])
    elif p[2] == '*=':
        p[0] = Tree('MULSET', Tree('ID', ref_var(make_Obst(p[1], cur_lev))), p[3])
    elif p[2] == '/=':
        p[0] = Tree('DIVSET', Tree('ID', ref_var(make_Obst(p[1], cur_lev))), p[3])



def p_logical_OR_expr(p):
    '''logical_OR_expr : logical_AND_expr
                       | logical_OR_expr OR logical_AND_expr'''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = Tree('OR', p[1], p[3])

def p_logical_AND_expr(p):
    '''logical_AND_expr : equality_expr
                        | logical_AND_expr AND equality_expr'''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = Tree('AND', p[1], p[3])

def p_equality_expr(p):
    '''equality_expr : relational_expr
                     | equality_expr EQ relational_expr
                     | equality_expr NE relational_expr'''
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 4 and p[2] == '==':
        p[0] = Tree('EQ', p[1], p[3])
    else:
        p[0] = Tree('NE', p[1], p[3])

def p_relational_expr(p):
    '''relational_expr : add_expr
                       | relational_expr LT add_expr
                       | relational_expr GT add_expr
                       | relational_expr LE add_expr
                       | relational_expr GE add_expr'''
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 4 and p[2] == '<':
        p[0] = Tree('LT', p[1], p[3])
    elif len(p) == 4 and p[2] == '>':
        p[0] = Tree('GT', p[1], p[3])
    elif len(p) == 4 and p[2] == '<=':
        p[0] = Tree('LE', p[1], p[3])
    else:
        p[0] = Tree('GE', p[1], p[3])

def p_add_expr(p):
    '''add_expr : mult_expr
                | add_expr PLUS mult_expr
                | add_expr MINUS mult_expr'''
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 4 and p[2] == '+':
        p[0] = Tree('PLUS', p[1], p[3])
    else:
        p[0] = Tree('MINUS', p[1], p[3])

def p_mult_expr(p):
    '''mult_expr : unary_expr
                 | mult_expr TIMES unary_expr
                 | mult_expr DIVIDE unary_expr
                 | mult_expr REMAINDER unary_expr'''
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 4 and p[2] == '*':
        p[0] = Tree('TIMES', p[1], p[3])
    elif len(p) == 4 and p[2] == '/':
        p[0] = Tree('DIVIDE', p[1], p[3])
    elif len(p) == 4 and p[2] == '%':
        p[0] = Tree('REMAINDER', p[1], p[3])

def p_unary_expr(p):
    '''unary_expr : postfix_expr
                  | MINUS unary_expr
                  | NOT unary_expr
                  | INC unary_expr
                  | DEC unary_expr'''
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 3 and p[1] == '-':
        p[0] = Tree('NEG', p[2])
    elif len(p) == 3 and p[1] == '++':
        p[0] = Tree('F_INC', p[2])
    elif len(p) == 3 and p[1] == '--':
        p[0] = Tree('F_DEC', p[2])
    else:
        p[0] = Tree('NOT', p[2])

def p_postfix_expr(p):
    '''postfix_expr : primary_expr
                    | IDENTIFIER LPAREN argument_expression_list RPAREN
                    | IDENTIFIER LPAREN RPAREN
                    | postfix_expr INC
                    | postfix_expr DEC'''
    global cur_lev
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 3 and p[2] == '++':
        p[0] = Tree('L_INC', p[1])
    elif len(p) == 3 and p[2] == '--':
        p[0] = Tree('L_DEC', p[1])
    elif len(p) == 5:
        argnum = count_param(p[3])
        p[0] = Tree('FCALL', Tree('ID', ref_fun(make_Obst(p[1], cur_lev), argnum)), p[3])
    else:
        argnum = 0
        p[0] = Tree('FCALL', Tree('ID', ref_fun(make_Obst(p[1], cur_lev), argnum)), None)

def p_primary_expr(p):
    '''primary_expr : IDENTIFIER
                    | CONSTANT
                    | LPAREN expression RPAREN'''

    if len(p) == 2 and isinstance(p[1], int):
        p[0] = Tree('CONSTANT', p[1])
    elif len(p) == 2 and isinstance(p[1], str):
        p[0] = Tree('ID', ref_var(make_Obst(p[1], cur_lev)))
    else:
        p[0] = p[2]

def p_argument_expression_list(p):
    '''argument_expression_list : assign_expr
                                | argument_expression_list COMMA assign_expr'''
    if len(p) == 2:
        p[0] = Tree('ASSIGN', p[1])
    else:
        p[0] = Tree('CONS', p[1], Tree('ASSIGN', p[3]))

def p_inc_lev(p):
    'inc_lev :'
    global cur_lev
    cur_lev += 1

def p_dec_lev(p):
    'dec_lev :'
    global cur_lev
    cur_lev -= 1
    Obst.pop(cur_lev)

def p_empty(p):
    'empty :'
    pass

def p_error(p):
    sys.stderr.write("SyntaxError in input\n")

parser = yacc.yacc()

def y_test():
    s = ""
    while True:
        try:
            s += input()
        except EOFError:
            break
        if not s:
            continue
    result = parser.parse(s)

    from tc_semantic import errcnt
    if errcnt != 0:
        sys.exit()
    # print_tree(result)
    print_code(result)


if __name__ == '__main__':
    y_test()

