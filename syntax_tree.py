from tc_semantic import Obst

class SyntaxTree:
    def __init__(self, op, *child):
        self.op = op
        self.child = child

def print_tree(t, tmp = ""):
    if isinstance(t, SyntaxTree):
        print(tmp + t.op + ':')
        tmp += '>   '
        for c in t.child:
            print_tree(c, tmp)
    else:
        if isinstance(t, Obst):
            print(tmp + "{0}, {1}, lv: {2}, offset: {3}, argnum: {4}, topalloc: {5}".format(t.name, t.kind, t.lev, t.offset, t.argnum, t.topalloc))
        else:
            print(tmp + str(t))

