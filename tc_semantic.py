import sys

errcnt = 0

class Obst:
    stack = [] # 名前空間スタック（クラス変数）

    def __init__(self, name, lev):
        self.name = name
        self.lev = lev
        self.kind = "FRESH"
        self.offset = None
        self.argnum = None # 関数の引数チェック
        self.topalloc = 0  # 関数ごとに局所変数の最小番地を記憶
        Obst.stack.insert(0, self) # スタックの頭に自分を追加

    def allocate_loc(self):
        # 相対番地割り当て
        tmp = 0
        if self.lev == 0:
            # Globalなら割り当てなし
            return None
        elif self.kind == "VAR":
            for o in Obst.stack:
                if o.kind == "VAR" and o.offset is not None:
                    tmp = o.offset - 4
                    break
            else:
                tmp =  -4
        elif self.kind == "PARAM":
            for o in Obst.stack:
                if o.kind == "PARAM" and o.offset is not None:
                    tmp =  o.offset + 4
                    break
            else:
                tmp = 8

        for o in Obst.stack[Obst.stack.index(self):]:
            if o.kind == 'FUN':
                if tmp < o.topalloc:
                    o.topalloc = tmp
                break

        return tmp

    @classmethod
    def pop(cls, l):
        # 現在より深いネストにある要素を全て吐き出す
        cls.stack = [o for o in cls.stack if o.lev <= l]

def lookup(name, lev):
    for o in Obst.stack:
        if name == o.name and lev >= o.lev:
            return o

def make_Obst(name, lev):
    tmp = lookup(name, lev)
    if tmp is None:
        return Obst(name, lev)
    else:
        return tmp

def make_var_decl(n, cur_lev):
    if n.kind == 'VAR':
        if n.lev == cur_lev:
            print_error("redeclaration of {}", n.name)

        n = Obst(n.name, cur_lev)

    elif n.kind in ('FUN', 'UNDEF_FUN'):
        if n.lev == cur_lev:
            print_error("redeclaration of {}", n.name)

        n = Obst(n.name, cur_lev)

    elif n.kind == 'PARAM':
        print_warn("declaration of {} shadows a parameter", n.name)
        n = Obst(n.name, cur_lev)

    elif n.kind == 'FRESH':
        pass

    n.kind = 'VAR'
    n.offset = n.allocate_loc()
    return n

def make_param_decl(n, cur_lev):
    if n.kind in ('VAR', 'FUN', 'UNDEF_FUN'):
        n = Obst(n.name, cur_lev)

    elif n.kind == 'PARAM':
        print_error("redeclaration of {}", n.name)
        return n

    elif n.kind == 'FRESH':
        pass

    n.kind = 'PARAM'

    for o in Obst.stack[Obst.stack.index(n):]:
        if o.kind == 'FUN':
            o.argnum += 1
            break

    n.offset = n.allocate_loc()
    return n

def make_fun_decl(n):
    if n.kind == 'VAR':
        print_error("{} redeclared as different kind of symbol", n.name)

    elif n.kind == 'FUN':
        print_error("redeclaration of {}", n.name)

    elif n.kind in ('UNDEF_FUN', 'FRESH'):
        n.kind = 'FUN'

    n.argnum = 0
    return n

def ref_var(n):
    if n.kind in ('VAR', 'PARAM'):
        pass
    elif n.kind in ('FUN', 'UNDEF_FUN'):
        print_error("function {} is used as variable", n.name)

    elif n.kind == 'FRESH':
        print_error("{} undeclared variable", n.name)
        n.kind = 'VAR'

    return n

def ref_fun(n, i):
    if n.kind in ('VAR', 'PARAM'):
        print_error("function {} is used as function", n.name)

    elif n.kind in ('FUN', 'UNDEF_FUN'):
        pass

    elif n.kind == 'FRESH':
        print_warn("{} undeclared function", n.name)
        n.kind = 'UNDEF_FUN'
        if n.lev > 0:
            globalize_sym(n)
        n.argnum = 0;

    if n.kind == 'FUN' and i != n.argnum:
        print_error("wrong number of param. {}", n.name)
    return n

def globalize_sym(n):
    tmp = Obst.stack.pop(Obst.stack.index(n))
    tmp.lev = 0
    Obst.stack.append(tmp)

def count_param(t): # 関数の引数の数チェック
    i = 0
    if t.op == 'ASSIGN':
        i += 1
        return i
    else:
        i += sum([count_param(c) for c in t.child])
        return i


def print_error(s, n):
    global errcnt
    errcnt += 1
    sys.stderr.write("ERROR" + str(errcnt) + ": " + s.format(n) + "\n")

def print_warn(s, n):
    sys.stderr.write("WARNING: " + s.format(n) + "\n")

